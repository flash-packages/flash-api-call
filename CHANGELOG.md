## 0.0.1

-   Initial release

## 0.0.2

-   Readme updated

## 0.0.3

-   Update: packages & migrations

## 0.0.4

-   Support: Mimetype 
-   Optimize: File upload

## 0.0.5

-   Fixed: Get api query params type error with `.toString()`
