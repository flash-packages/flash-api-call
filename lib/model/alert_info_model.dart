part of flash_api_call;

class AlertInfo {
  final String title;
  final String message;
  AlertInfo({
    required this.title,
    required this.message,
  });
}
