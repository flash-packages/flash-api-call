part of flash_api_call;

extension APIRequestInfoExtension on APIRequestInfo {
  Object? get getBody => (parameter is Map?) || (parameter is Map)
      ? json.encode(parameter)
      : parameter;

  Map<String, dynamic>? get getParams => (parameter is Map) && (parameter != null)
      ? (parameter! as Map<String, dynamic>)
          .map((key, value) => MapEntry(key, value.toString()))
      : null;
}
