part of flash_api_call;

class FlashAPICall {
  // Instance
  static FlashApiCallInstance get instance => FlashApiCallInstance.apiInstance;

  // Check internet connectivity
  static Future<bool> checkConnectivity() => instance.checkConnectivity();
}
