part of flash_api_call;

class FlashException implements Exception {
  final String? message;
  final String? title;
  final ExceptionType type;
  final int statusCode;

  FlashException({
    this.title,
    this.message,
    this.type = ExceptionType.none,
    this.statusCode = 200,
  });

  AlertInfo get getAlertInfo {
    String alertTitle = "";
    String msg = "";

    switch (type) {
      // No Internet
      case ExceptionType.noInternet:
        alertTitle = APIErrorMsg.noInternet;
        msg = APIErrorMsg.noInternetMsg;
        break;

      // Unauthorized
      case ExceptionType.unauthorized:
        alertTitle = title ?? APIErrorMsg.unauthorizedTitle;
        msg = message ?? APIErrorMsg.unauthorizedMsg;
        break;

      // HTTP Exception
      case ExceptionType.httpException:
        alertTitle = title ?? APIErrorMsg.defaultErrorTitle;
        msg = message ?? APIErrorMsg.somethingWentWrong;
        break;

      // Format Exception (Don't know response type or schema)
      case ExceptionType.formatException:
        alertTitle = title ?? APIErrorMsg.defaultErrorTitle;
        msg = message ?? APIErrorMsg.somethingWentWrong;
        break;

      // General Error
      case ExceptionType.none:
        alertTitle = title ?? APIErrorMsg.defaultErrorTitle;
        msg = message ?? APIErrorMsg.somethingWentWrong;
        break;

      // Under Maintainance
      case ExceptionType.underMaintainance:
        alertTitle = title ?? APIErrorMsg.underMaintainanceTitle;
        msg = message ?? APIErrorMsg.underMaintainanceMsg;
        break;

      // Timeout
      case ExceptionType.timeOut:
        alertTitle = title ?? APIErrorMsg.requestTimeoutTitle;
        msg = message ?? APIErrorMsg.requestTimeoutMessage;
        break;
    }
    return AlertInfo(title: alertTitle, message: msg);
  }

  String get getTitle => getAlertInfo.title;
  String get getMessage => getAlertInfo.message;
}