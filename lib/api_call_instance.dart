part of flash_api_call;

class FlashApiCallInstance {
  FlashApiCallInstance._();
  static FlashApiCallInstance get apiInstance => FlashApiCallInstance._();

  // Call API
  Future<http.Response> callService({
    required APIRequestInfo requestInfo,
  }) async {
    try {
      // Check internet
      await checkConnectivity();

      // Print request info
      if (kDebugMode) {
        _printApiDetial(requestInfo);
      }

      // Get response
      return requestInfo.files.isEmpty
          ? await _callAPI(requestInfo: requestInfo)
              .timeout(Duration(seconds: requestInfo.timeSecond))
          : await _callMultipartAPI(requestInfo: requestInfo)
              .timeout(Duration(seconds: requestInfo.timeSecond));

      // Exceptions
    } on SocketException catch (e) {
      throw FlashException(
        message: e.message,
        type: ExceptionType.noInternet,
      );
    } on HttpException catch (e) {
      throw FlashException(
        message: e.message,
        type: ExceptionType.httpException,
      );
    } on FormatException catch (e) {
      throw FlashException(
        message: e.source?.toString(),
        type: ExceptionType.formatException,
      );
    } on TimeoutException {
      throw FlashException(
        title: APIErrorMsg.requestTimeoutTitle,
        message: APIErrorMsg.requestTimeoutMessage,
        type: ExceptionType.timeOut,
      );
    } catch (error) {
      rethrow;
    }
  }

  // Check internet connectivity
  Future<bool> checkConnectivity() async {
    ConnectivityResult connectivity = await Connectivity().checkConnectivity();
    if (connectivity == ConnectivityResult.none) {
      throw FlashException(
        title: APIErrorMsg.noInternet,
        message: APIErrorMsg.noInternetMsg,
      );
    }
    return true;
  }

  // Call API
  Future<http.Response> _callAPI({
    required APIRequestInfo requestInfo,
  }) async {
    // final URL
    String url = requestInfo.url;

    // Http response
    http.Response response;

    // Add header
    Map<String, String>? apiHeader = requestInfo.headers;

    // Call API with respect to request type
    switch (requestInfo.requestType) {
      case HTTPRequestMethod.post:
        response = await http.post(
          Uri.parse(url),
          body: requestInfo.getBody,
          headers: apiHeader,
        );
        break;
      case HTTPRequestMethod.get:
        Uri url0 = Uri.parse(url);
        response = await http.get(
          url0.replace(queryParameters: requestInfo.getParams),
          headers: apiHeader,
        );
        break;
      case HTTPRequestMethod.delete:
        response = await http.delete(
          Uri.parse(url),
          body: requestInfo.getBody,
          headers: apiHeader,
        );
        break;
      case HTTPRequestMethod.put:
        response = await http.put(
          Uri.parse(url),
          body: requestInfo.getBody,
          headers: apiHeader,
        );
        break;
      case HTTPRequestMethod.patch:
        response = await http.patch(
          Uri.parse(url),
          body: requestInfo.getBody,
          headers: apiHeader,
        );
        break;
    }

    // Print request info
    if (kDebugMode) {
      _printResponse(response, requestInfo.serviceName);
    }

    // Return received response
    return response;
  }

  // Multipart request
  Future<http.Response> _callMultipartAPI({
    required APIRequestInfo requestInfo,
  }) async {
    // Get URI
    Uri uri = Uri.parse(requestInfo.url);
    http.MultipartRequest request = http.MultipartRequest(
      requestInfo.requestType.name.toUpperCase(),
      uri,
    );

    // Add parameters
    if ((requestInfo.parameter is Map?) || (requestInfo.parameter is Map)) {
      (requestInfo.parameter as Map<String, dynamic>?)
          ?.forEach((key, value) => request.fields[key] = value);
    } else {
      request.fields.addAll(jsonDecode(requestInfo.parameter as String));
    }

    // Add header
    Map<String, String>? apiHeader = requestInfo.headers;
    apiHeader?.forEach((key, value) => request.headers[key] = value);

    // Set documents
    List<Future<http.MultipartFile>> files = requestInfo.files
        .map(
          (docInfo) => docInfo.docPathList.map(
            (doc) => http.MultipartFile.fromPath(
              docInfo.docKey,
              doc.docPath,
              filename: (doc.docPath),
              contentType: MediaType.parse(doc.mimeType),
            ).catchError(
              (error) {
                if (kDebugMode) {
                  log("-----------------Error While uploading Image: ${doc.docPath}, Error: $error -----------------");
                }
                return [];
              },
            ),
          ),
        )
        .expand((multipartFile) => multipartFile)
        .toList();

    // Upload all files
    List<http.MultipartFile> multiPartFiles =
        await Future.wait<http.MultipartFile>(files);
    request.files.addAll(multiPartFiles);

    // Send request
    http.Response response =
        await http.Response.fromStream(await request.send());

    // Print request info
    if (kDebugMode) {
      _printResponse(response, requestInfo.serviceName);
    }

    // Return received response
    return response;
  }

  // API info
  void _printApiDetial(APIRequestInfo info) {
    String apiLog = """

        ${info.serviceName} Service Parameters
        |-------------------------------------------------------------------------------------------------------------------------
        | Method :- ${info.requestType.name.toUpperCase()}
        | URL     :- ${info.url}
        | Header  :- ${info.headers}
        | Params  :- ${info.parameter}
        |-------------------------------------------------------------------------------------------------------------------------
        """;
    log(apiLog);
  }

  // API resposne info
  void _printResponse(http.Response response, String serviceName) {
    String apiLog = """

        $serviceName Service Response
        |--------------------------------------------------------------------------------------------------------------------------
        | API        :- $serviceName
        | StatusCode :- ${response.statusCode}
        | Body    :- ${response.body}
        |--------------------------------------------------------------------------------------------------------------------------
        """;
    log(apiLog);
  }
}

// API request object
class APIRequestInfo {
  HTTPRequestMethod requestType;
  String url;
  Object? parameter;
  Map<String, String>? headers;
  List<UploadDocument> files;
  String serviceName;
  int timeSecond = 90;

  APIRequestInfo({
    this.requestType = HTTPRequestMethod.get,
    this.parameter,
    this.headers,
    this.files = const [],
    required this.url,
    this.serviceName = "",
    this.timeSecond = 90,
  });
}

// Uploading document object

class UploadDocument {
  String docKey;
  List<FlashDocument> docPathList;

  UploadDocument({
    this.docKey = "",
    this.docPathList = const [],
  });
}

class FlashDocument {
  String docPath;
  String mimeType;

  FlashDocument({
    this.docPath = "",
    this.mimeType = "application/octet-stream",
  });
}
