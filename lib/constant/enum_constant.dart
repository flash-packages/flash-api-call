part of flash_api_call;

// API exceptions
enum ExceptionType {
  noInternet,
  httpException,
  formatException,
  unauthorized,
  underMaintainance,
  timeOut,
  none,
}

// HTTP request type
enum HTTPRequestMethod {
  get,
  post,
  delete,
  put,
  patch,
}
