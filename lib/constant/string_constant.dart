part of flash_api_call;

class APIErrorMsg {
  static const String socketException = 'Please try again later.';

  // No Internet
  static const String noInternet = "No Internet";
  static const String noInternetMsg = "Please check your internet connection.";

  // Unauthorized user
  static const String unauthorizedTitle = "Unauthorized";
  static const String unauthorizedMsg =
      "Unauthorized! you are not allowed to do this action.";

  // Default error msg
  static const String defaultErrorTitle = "Error";
  static const String somethingWentWrong =
      "Something went wrong, please try again later";

  // Under maintiance
  static const String underMaintainanceTitle = "Under Maintainance";
  static const String underMaintainanceMsg =
      "Sorry, We're down for scheduled maintenance right now, please try after some time.";

  // Invalid API response format
  static const String invalidFormat = "Invalid format";
  static const String httpErrorMsg = "The server is currently unavailable.";

  // Request timeout
  static const String requestTimeoutTitle = "Request Timeout";
  static const String requestTimeoutMessage =
      "Looks like the server is taking too long to respond. This can be caused by poor connectivity or an issue with our servers. Please try again in a moment.";
}
