# flash_api_call

[![pub package](https://img.shields.io/pub/v/flash_api_call.svg)](https://pub.dev/packages/flash_api_call)

A Flutte package which makes API call easier and maintainable.
Supports Android, iOS, Linux, macOS and Windows.
Not all methods are supported on all platforms.

|             | Android | iOS  | Linux | macOS  | Windows     |
| ----------- | ------- | ---- | ----- | ------ | ----------- |
| **Support** | SDK 16+ | 9.0+ | Any   | 10.11+ | Windows 10+ |

## Usage

To use this plugin, add `flash_api_call` as a [dependency in your pubspec.yaml file](https://flutter.dev/docs/development/platform-integration/platform-channels).

## Example

<?code-excerpt "readme_excerpts.dart (Example)"?>

```dart
// TODO
```

See this `flash_api_call` [test](https://gitlab.com/flash-packages/flash_api_call/-/tree/main/example/lib/main.dart) for an example.

A Codeflash's Product ✨
